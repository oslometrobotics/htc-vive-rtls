# HTC Vive Real-Time-Location-System #


### Information on HTC vive lighthouse system ###

* http://hackaday.com/2016/12/21/alan-yates-why-valves-lighthouse-cant-work/
* http://diydrones.com/profiles/blogs/indoor-drone-localization-with-htc-vive-lighthouse
* https://trmm.net/Lighthouse
* [Presentation of Alan Yates on Lighthouse system](https://youtu.be/75ZytcYANTA)
* [Interview to the creators of lighthouse "The Secret Prototypes of Valve's VR Lab"](https://www.youtube.com/watch?v=QLBxz7djQvc)



### Other open source project repositories ###

* https://github.com/nairol/LighthouseRedox
* https://github.com/ashtuchkin/vive-diy-position-sensor
* https://github.com/osresearch/lighthouse
* https://github.com/cnlohr/libsurvive

### Links on algorithms ###

* [Least-Squares Intersection of Lines](http://cal.cs.illinois.edu/~johannes/research/LS_line_intersect.pdf)
* https://github.com/cnlohr/libsurvive/issues/3
* https://en.wikipedia.org/wiki/Perspective-n-Point
* https://en.wikipedia.org/wiki/Epipolar_geometry