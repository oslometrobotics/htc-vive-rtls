

```python
%matplotlib notebook

import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import scipy.optimize as opt
```


```python
## Rotation matrices
def rotate_x(ang):
    return np.array([[1,0,0],[0,np.cos(ang),-np.sin(ang)],[0,np.sin(ang),np.cos(ang)]])

def rotate_y(ang):
    return np.array([[np.cos(ang),0,np.sin(ang)],[0,1,0],[-np.sin(ang),0,np.cos(ang)]])

def rotate_z(ang):
    return np.array([[np.cos(ang),-np.sin(ang),0],[np.sin(ang),np.cos(ang),0],[0,0,1]])

def rotate_zyx(x,y,z):
    #return np.matmul(rotate_z(z),np.matmul(rotate_y(y),rotate_x(x)))
    return np.matmul(rotate_x(x),np.matmul(rotate_y(y),rotate_z(z)))

## Rotation helpers
def transform_to_lh_view(pt, pose):
    rotation = rotate_zyx(pose[3],pose[4],pose[5])
    return np.matmul(pt-pose[0:3],rotation)

def measure_samples(samples, pose):
    output = np.zeros_like(samples)
    for i in range(0,samples.shape[1]):
        output[...,i] = transform_to_lh_view(samples[...,i],pose)
        output[...,i] = output[...,i]/np.linalg.norm(output[...,i])
    return output

## Distance between lines
def line_distance(P,u,Q,v):
    w0 = np.array(P)-np.array(Q)
    a = np.dot(u,u)
    b = np.dot(u,v)
    c = np.dot(v,v)
    d = np.dot(u,w0)
    e = np.dot(v,w0)
    
    return np.linalg.norm(w0 + ((b*e-c*d)*u-(a*e-b*d)*v)/(a*c-b*b))
```


```python
## Plotting helper functions
def transform_to_pose(vec, pose):
    rotated = np.matmul(rotate_zyx(pose[3],pose[4],pose[5]),vec)
    return rotated+pose[0:3]

def plot_axes(ax, pose, color):
    scale = 0.5
    ps = np.array([[0,0,1],[0,0,0],[0,1,0],[0,0,0],[1,0,0]]).T
    ps = scale*ps
    
    transformed = np.zeros_like(ps)
    for i in range(0,ps.shape[1]):
        transformed[...,i] = transform_to_pose(ps[...,i],pose)
        
    ax.plot(transformed[0,...],transformed[1,...],transformed[2,...],'-o',markersize=5,markevery=10,color=color)
    
def plot_measured_lines(ax, pose, samples, color, length):
    rotation = rotate_zyx(pose[3],pose[4],pose[5])
    measured = measure_samples(samples, pose)
    for i in range(0,measured.shape[1]):
        line = measured[...,i]/np.linalg.norm(measured[...,i])*length
        rotated = np.matmul(rotation,line)+pose[0:3]

        ax.plot([pose[0],rotated[0]],[pose[1],rotated[1]],[pose[2],rotated[2]],'--',color=color)

```


```python
## Create positions for Lighthouses
p1 = [0, 0, 0, 0, 0, 0]
p2 = [2, 0.2, 2, 0.5, 3*np.pi/2, np.pi]

# Random points (in view)
N_points = 20
points_range = [[0.5,1.5], [-0.5,0.5], [0.5,1.5]]

samples = np.random.rand(3,N_points)
samples[0,...] = samples[0,...]*(points_range[0][1]-points_range[0][0])+points_range[0][0]
samples[1,...] = samples[1,...]*(points_range[1][1]-points_range[1][0])+points_range[1][0]
samples[2,...] = samples[2,...]*(points_range[2][1]-points_range[2][0])+points_range[2][0]

# Measure vectors from Lighthouses
meas1 = measure_samples(samples, p1)
meas2 = measure_samples(samples, p2)
```


```python
## Plot
fig1 = plt.figure()
ax1 = fig1.add_subplot(111, projection='3d')

# Poses of LH
plot_axes(ax1,p1,'b')
plot_axes(ax1,p2,'g')

# Samples points
ax1.plot(samples[0,...],samples[1,...],samples[2,...],'o',color='k')

# Measured lines
plot_measured_lines(ax1,p1,samples,'k',3)
plot_measured_lines(ax1,p2,samples,'k',3)

ax1.set_xlim([0,2])
ax1.set_ylim([-1,1])
ax1.set_zlim([0,2])
```

![generated-sample-points](pictures/generated-sample-points.png)

```python
## Solve optimization problem
# Initial guess
q1 = [0, 0, 0, 0, 0, 0]
q2 = [1/np.sqrt(2), 0, 1/np.sqrt(2), 0, 3*np.pi/2, np.pi] # Pretty good guess to start with
#q2 = [0, 0, 0, 0, 0, 0]

# Objective function
def objective(pose):
    sum = 0
    P = q1[0:3]
    Q = pose[0:3]
    rotation = rotate_zyx(pose[3],pose[4],pose[5])
    
    for i in range(0,samples.shape[1]):
        u = meas1[...,i]
        v = np.matmul(rotation,meas2[...,i])
        sum += line_distance(P,u,Q,v)

    return sum

# Constraints
def distance(pose):
    return np.linalg.norm(pose[0:3])-1
cstr_distance = { 'type': 'eq', 'fun': distance }

def point_opposite(pose):
    rotation = rotate_zyx(pose[3],pose[4],pose[5])
    z1 = np.array([0,0,1])
    z2 = np.matmul(rotation, np.array([0,0,1]))
    return -np.dot(z1,z2)
cstr_point_opposite = { 'type': 'ineq', 'fun': point_opposite}

def point_towards(pose):
    rotation = rotate_zyx(pose[3],pose[4],pose[5])
    z1 = np.array(pose[0:3])
    z2 = np.matmul(rotation, np.array([0,0,1]))
    return -np.dot(z1,z2)
cstr_point_towards = { 'type': 'ineq', 'fun': point_towards}

# Bounds (translation positive in z - in front of other lighhouse, and rotations in [0,2*pi])
bounds = [
    (-1, 1),
    (-1, 1),
    (0, 1),
    (0, 2*np.pi),
    (0, 2*np.pi),
    (0, 2*np.pi)
]

## Do optimization
res = opt.minimize(objective,q2,
             method='SLSQP',
             jac=False,
             bounds=bounds,
             constraints=[cstr_distance,cstr_point_opposite,cstr_point_towards],
             options={'disp': True, 'ftol': 1e-9, 'maxiter': 1000}
)

#print(res)
```

    Optimization terminated successfully.    (Exit mode 0)
                Current function value: 2.3774415002e-06
                Iterations: 34
                Function evaluations: 331
                Gradient evaluations: 34
    


```python
## Plot resulting estimate
fig2 = plt.figure()
ax2 = fig2.add_subplot(111, projection='3d')

# Shift estimate to position of 2nd LH
pest1 = res['x']
pest2 = np.concatenate((pest1[0:3]*np.linalg.norm(p2[0:3]), pest1[3:6]))

# Poses of LH
plot_axes(ax2,p1,'b')
plot_axes(ax2,p2,'g')
#plot_axes(ax2,pest1,'c')
plot_axes(ax2,pest2,'r')

ax2.set_xlim([0,2])
ax2.set_ylim([-1,1])
ax2.set_zlim([0,2])
```

![solved-orientation](pictures/solved-orientation.png)