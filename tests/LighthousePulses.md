## Received Lighthouse pulses
The following plot shows the received light pulses from two Vive base-stations, with the time (in us) along the x-axis, and the length of the pulse (in us) on the y-axis.
The two synchronization pulses just after eachother, and the shorter laser sweep pulses in beteen are clearly visible.

![Pulse width vs. time](pictures/pulse-timing-vs-width.png)

The next plot shows the distribution of pulse widths over a ~1.5s sampling period of the same signal as above.
The laser sweep pulses are between 10-20us long, and the eight different bins of the sync pulse data is clearly visible between 60-140us in width.

![Pulse width histogram](pictures/pulse-width-hist.png)