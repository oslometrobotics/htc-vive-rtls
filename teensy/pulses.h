/*
 * pulses.h
 *
 *  Created on: 21. feb. 2017
 *      Author: jakobho
 */

#ifndef PULSES_H_
#define PULSES_H_

void decodePulse(uint32_t time, uint32_t width);

#endif /* PULSES_H_ */
