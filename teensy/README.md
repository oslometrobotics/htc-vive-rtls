## HTC Vive lighthouse-decoder with the Teensy 3.2

### Pins in use
* PC3 (Pin 9) - COMP1_IN1 - Connect to output of the IR sensor
* PC4 (Pin 10) - COMP1_OUT - Jumper to pin 6
* PD4 (Pin 6) - FTM0_CH4 - Input to pulse counter

### Hardware documentation
See [K20P64M72SF1RM](http://www.nxp.com/assets/documents/data/en/reference-manuals/K20P64M72SF1RM.pdf) (mcu reference manual) for pins, functional and register descriptions.

See [sensor schematic](https://github.com/ashtuchkin/vive-diy-position-sensor/blob/master/docs/sensor-schematics.svg) for how to build the IR sensor with amplifier.
