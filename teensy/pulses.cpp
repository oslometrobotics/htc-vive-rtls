/*
 * pulses.cpp
 *
 *  Created on: 21. feb. 2017
 *      Author: jakobho
 */

#include "core/WProgram.h"
#include "pulses.h"

/*
 *  Pulse lengths:
 *  < 60 us => Laser Sweep
 *
 *  Length (us) | Skip | Data | Axis
 *  ---------------------------------
 *   60 -  71   |   0  |   0  |   0
 *   71 -  82   |   0  |   0  |   1
 *   82 -  93   |   0  |   1  |   0
 *   93 - 103   |   0  |   1  |   1
 *  103 - 113   |   1  |   0  |   0
 *  113 - 123   |   1  |   0  |   1
 *  123 - 134   |   1  |   1  |   0
 *  134 - 144   |   1  |   1  |   1
 *
 *  For more info, see:
 *  https://github.com/nairol/LighthouseRedox/blob/master/docs/Light%20Emissions.md
 *  https://github.com/SINTEF-9012/HiOA-drone-project-2017/blob/master/Tracking/Vive/docs/LighthousePulses.md
 */

#define US_TO_CLOCK(us) ((us)*32)

uint32_t lastSyncPulse = 0;
inline uint32_t timeSinceLastSync(uint32_t time, bool set)
{
	uint32_t result = 0;
	if (time > lastSyncPulse) result = time-lastSyncPulse;
	else result = 0xFFFFFFFF-lastSyncPulse+time;
	if (set) lastSyncPulse = time;
	return result;
}

inline int widthToAxis(uint32_t width)
{
	if (width < US_TO_CLOCK(60)) {
		//GPIOC_PTOR  = 0x0080; // Toggle debug 1 (D1)
		return -1;
	}
	else if (width < US_TO_CLOCK(71)) return 0;
	else if (width < US_TO_CLOCK(82)) return 0;
	else if (width < US_TO_CLOCK(93)) return 0;
	else if (width < US_TO_CLOCK(103)) return 0;
	else if (width < US_TO_CLOCK(113)) return 0;
	else if (width < US_TO_CLOCK(123)) return 0;
	else if (width < US_TO_CLOCK(134)) return 0;
	else if (width < US_TO_CLOCK(144)) return 0;
	else {
		GPIOC_PTOR  = 0x0080; // Toggle debug 1 (D1)
		//if (width >= US_TO_CLOCK(170)) GPIOC_PTOR  = 0x0080; // Toggle debug 1 (D1)
		return -1;
	}
}

inline int widthToSkip(uint32_t width)
{
	if (width >= US_TO_CLOCK(60) && width < US_TO_CLOCK(103)) return 0;
	else if (width >= US_TO_CLOCK(103) && width < US_TO_CLOCK(144)) return 1;
	else return -1;
}

struct {
	uint32_t MasterStart = 0;
	int MasterAxis = -1;
	int MasterSkip = -1;
	uint32_t SlaveStart = 0;
	int SlaveAxis = -1;
	int SlaveSkip = -1;
} frame;

void decodePulse(uint32_t time, uint32_t width)
{
	GPIOC_PTOR  = 0x0020; // Toggle LED
	if (width >= US_TO_CLOCK(60)) {
		/* ----- A sync pulse ----- */
		uint32_t last = timeSinceLastSync(time, true);

		if (last > US_TO_CLOCK(500)) {
			GPIOC_PTOR  = 0x0040; // Toggle debug 2 (D2)
			// A long time since we have seen a sync pulse, so this has to be the start of a frame
			frame.MasterStart = time;
			frame.MasterAxis = widthToAxis(width);
			frame.MasterSkip = widthToSkip(width);
			frame.SlaveStart = 0;
			frame.SlaveAxis = -1;
			frame.SlaveSkip = -1;

		} else if (last > US_TO_CLOCK(300) && frame.MasterAxis >= 0) {
			// Perfect timing for the second sync pulse, and we have got the first pulse
			frame.SlaveStart = time;
			frame.SlaveAxis = widthToAxis(width);
			frame.SlaveSkip = widthToSkip(width);

		} else {
			// Either sync pulses appeared to close, or we didn't see the first one
			// Reset everything and bail out
			frame.MasterStart = 0;
			frame.MasterAxis = -1;
			frame.MasterSkip = -1;
			frame.SlaveAxis = -1;
			frame.SlaveSkip = -1;

		}
	} else {
		//GPIOC_PTOR  = 0x0080; // Toggle debug
		//if (frame.MasterAxis >= 0) GPIOC_PTOR  = 0x0020; // Toggle LED
		/* ----- A laser sweep ----- */
		if (frame.MasterAxis >= 0 && frame.SlaveAxis >= 0 && timeSinceLastSync(time, false) < US_TO_CLOCK(8333)) {
			// We did see both sync pulses, and the laser sweep inside one frame
			// All good!

			//GPIOC_PTOR  = 0x0020; // Toggle LED

		}

		// Reset
		frame.MasterStart = 0;
		frame.MasterAxis = -1;
		frame.MasterSkip = -1;
		frame.SlaveAxis = -1;
		frame.SlaveSkip = -1;
	}
	Serial.println(width);
}
